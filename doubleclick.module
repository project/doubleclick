<?php

/**
 * @file
 *  Contains Drupal hooks and form functions for the DoubleClick project
 * @todo
 *  - Split organic groups integration into seperate module
 */

define('DOUBLECLICK_DEFAULT_AD_SOURCE', 'http://ad.doubleclick.net/adj');
define('DOUBLECLICK_DEFAULT_NOSCRIPT_SOURCE', 'http://ad.doubleclick.net/ad');
define('DOUBLECLICK_DEFAULT_AD_HREF', 'http://ad.doubleclick.net/jump');
define('DOUBLECLICK_DEFAULT_SITENAME', $_SERVER['SERVER_NAME']);

/**
 * Implements hook_menu().
 */
function doubleclick_menu() {
  $items['admin/content/doubleclick'] = array(
    'title' => 'DoubleClick',
    'page callback' => 'doubleclick_admin_list_form',
    'access arguments' => array('advertising managment'),
  );

  $items['admin/content/doubleclick/list'] = array(
    'title' => 'List Slots',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  
  $items['admin/content/doubleclick/test'] = array(
    'title' => 'Test Page',
    'type' => MENU_LOCAL_TASK,
    'weight' => 0,
    'access arguments' => array('advertising managment'),
    'page callback' => 'doubleclick_admin_test_page',
  );

  $items['admin/content/doubleclick/add'] = array(
    'title' => 'Add Slot',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_form'),
    'access arguments' => array('advertising managment'),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/content/doubleclick/edit'] = array(
    'title' => 'Edit Slot',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_form'),
    'access arguments' => array('advertising managment'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/content/doubleclick/edit/%slotid'] = array(
    'title' => 'Edit Slot',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_form', 3),
    'access arguments' => array('advertising managment'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/content/doubleclick/delete'] = array(
    'title' => 'Are you sure you want to delete this slot from the system ?.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_delete'),
    'access arguments' => array('advertising managment'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/content/doubleclick/delete/%slotname'] = array(
    'title' => 'Are you sure you want to delete this slot from the system ?',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_delete', 3),
    'access arguments' => array('advertising managment'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/doubleclick'] = array(
    'title' => 'Doubleclick',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('build_doubleclick_admin_settings_form'),
    'access arguments' => array('advertising managment'),
  );

  // AHAH callback for adding more size options
  $items['doubleclick/js'] = array(
    'title' => 'Doubleclick AHAH',
    'access arguments' => array('access content'),
    'page callback' => 'doubleclick_sizes_js',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function doubleclick_permission() {
  return array(
    'advertising managment' => array(
      'title' => t('advertising managment'),
      'description' => t('TODO Add a description for \'advertising managment\''),
    ),
  );
}

/**
 * Implements hook_help().
 */
function doubleclick_help($path) {
  $output = '';
  switch ($path) {
    case 'admin/content/doubleclick/add':
      $output = 'This page gives you the ability to create new slots blocks to assign them in the page.';
      break;
    case 'admin/content/doubleclick/list':
    case 'admin/content/doubleclick':
      $output = 'View all the slots in the system.';
      break;
    case 'admin/config/doubleclick':
      $output = 'Set the basic information for all slots in the system.';
      break;
  }
  return $output;
}

/**
 * Form builder function for slot add / edit form
 */
function build_doubleclick_admin_form($form, &$form_state, $slotid = '') {
  drupal_add_css(drupal_get_path('module', 'doubleclick') . '/doubleclick.css');
  $slot = doubleclick_slot_load($slotid);

  $form = array(
    '#id' => 'doubleclick-admin-form',
    '#redirect' => 'admin/content/doubleclick',
    '#validate' => array('build_doubleclick_admin_validate'),
    '#submit' => array('build_doubleclick_admin_submit'),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save slot'),
      '#weight' => 100,
    ),
    'sid' => array(
      '#type' => 'hidden',
      '#value' => $slot->sid,
    ),
  );

  $form['add_slot'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Slot'),
    '#description' => t('Add new slot to the site.'),
  );

  $form['add_slot']['basic_information'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic Information'),
  );

  $form['add_slot']['basic_information']['friendly_name'] = array(
    '#title' => t('Friendly Name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size' => '40',
    '#description' => 'Suggested values : killer,leaderboard,overlay...',
    '#default_value' => $slot->friendly_name,
  );

  $form['add_slot']['basic_information']['zone_name'] = array(
    '#title' => t('Zone Name'),
    '#description' => t('The machine name for this slot.'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#size' => '40',
    '#default_value' => $slot->zone_name,
  );

  $form['add_slot']['sizes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slot Size'),
  );
  $form['add_slot']['sizes']['sizes_more'] = array(
    '#type' => 'submit',
    '#value' => t('Add another size'),
    '#description' => t('Click here if you need to add another size option.'),
    '#weight' => 10,
    '#ahah' => array(
      'path' => 'doubleclick/js',
      'wrapper' => 'doubleclick-sizes',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#submit' => array('doubleclick_sizes_more_submit'), // Handles submit when JS not enabled
  );
  $form['add_slot']['sizes']['size_wrapper'] = array(
    '#prefix' => '<div id="doubleclick-sizes">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  // Supporting multiple ad sizes
  if ($form_state['current_sizes']) {
    $count = count($form_state['current_sizes']);
  }
  else {
    $count = count($slot->sizes);
  }
  for ($delta = 0; $delta <= $count; $delta++) {
    $form['add_slot']['sizes']['size_wrapper'][$delta] = array(
      '#prefix' => '<div class="size-wrapper"><h4>' . $delta . '</h4>',
      '#suffix' => '</div><div class="clear-block"></div>',
    );

    $width = (!empty($slot->sizes[$delta]['width'])) ? $slot->sizes[$delta]['width'] : $form_state['current_sizes'][$delta]['width'];
    $height = (!empty($slot->sizes[$delta]['height'])) ? $slot->sizes[$delta]['height'] : $form_state['current_sizes'][$delta]['height'];
    $form['add_slot']['sizes']['size_wrapper'][$delta]['width'] = array(
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#required' => ($delta === 0) ? TRUE : FALSE,
      '#size' => '5',
      '#default_value' => $width,
    );

    $form['add_slot']['sizes']['size_wrapper'][$delta]['height'] = array(
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#required' => ($delta === 0) ? TRUE : FALSE,
      '#size' => '5',
      '#default_value' => $height,
    );
  }

  $form['add_slot']['targeting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Targeting settings'),
  );

  $form['add_slot']['targeting']['kv'] = array(
    '#title' => t('Targeting'),
    '#type' => 'textarea',
    '#rows' => '5',
    '#default_value' => $slot->kv,
    '#description' => 'Targeting for this slot write one each line in this form (key|value) example (city|amman)',
  );

  $form['add_slot']['more_information'] = array(
    '#type' => 'fieldset',
    '#title' => t('More Information'),
    '#collapsible' => TRUE,
  );

  $form['add_slot']['more_information']['is_overlay'] = array(
    '#title' => t('Is Overlay'),
    '#type' => 'checkbox',
    '#default_value' => $slot->is_overlay,
  );

  $form['add_slot']['more_information']['tile'] = array(
    '#title' => t('Tile number'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#size' => '5',
    '#default_value' => $slot->tile,
  );

  $form['add_slot']['more_information']['position'] = array(
    '#title' => t('Position'),
    '#type' => 'select',
    '#options' => array(
      'default' => 'Not specified',
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
    '#default_value' => $slot->position,
    '#required' => FALSE,
  );

  $form['add_slot']['more_information']['ord'] = array(
    '#title' => t('Ord'),
    '#type' => 'textfield',
    '#default_value' => $slot->ord,
  );

  return $form;
}

/**
 * Validation handler for slot add / edit form
 */
function build_doubleclick_admin_validate($form, &$form_state) {
  if (is_array($form_state['values']['size_wrapper'])) {
    foreach ($form_state['values']['size_wrapper'] as $delta => $size) {
      // Make sure there is a value in there
      if (empty($size['width']) && empty($size['height'])) {
        unset($form_state['values']['size_wrapper'][$delta]);
        continue;
      }

      // Make sure they entered a value for both if any
      if (empty($size['width']) || empty($size['height'])) {
        form_set_error('size_wrapper][' . $delta . '][height', t('You must enter a both a width and height value.'));
        form_set_error('size_wrapper][' . $delta . '][width', '');
      }

      // Make sure they only entered numeric values
      foreach ($size as $type => $value) {
        if (!is_numeric($value)) {
          form_set_error('size_wrapper][' . $delta . '][' . $type, t('You must enter a numeric value for dimensions.'));
        }
      }
    }
  }
}

/**
 * Submit handler for slot add / edit form
 */
function build_doubleclick_admin_submit($form, &$form_state) {
  $slot = build_slot_object($form_state);
  doubleclick_slot_save($slot);
}

/**
 * Form builder for settings page
 */
function build_doubleclick_admin_settings_form() {
  $form = array(
    '#id' => 'doubleclick-admin-settings-form',
    '#submit' => array('doubleclick_admin_settings_form_submit'),
    'submit' => array(
      '#value' => t('Save Settings'),
      '#type' => 'submit',
      '#weight' => 100,
    ),
  );

  $form['site_name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site name'),
    '#description' => t('Configure the site name that will appended to the url of each slot "' . variable_get('doubleclick_ad_source', DOUBLECLICK_DEFAULT_AD_SOURCE) . '/~SITE NAME~/SLOT NAME"'),
  );
  $form['site_name']['site_name_text_simple'] = array(
    '#type' => 'textfield',
    '#title' => t('Simple Site Name'),
    '#description' => 'enter the sitename that will appended to all slots',
    '#required' => TRUE,
    '#default_value' => variable_get('doubleclick_sitename', DOUBLECLICK_DEFAULT_SITENAME),
  );

  $form['urls'] = array(
    '#type' => 'fieldset',
    '#title' => t('Doubleclick URL Settings'),
    '#description' => t('The default settings should be fine for USA DoubleClick customers.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['urls']['ad_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Advertisement source server'),
    '#default_value' => variable_get('doubleclick_ad_source', DOUBLECLICK_DEFAULT_AD_SOURCE),
  );
  $form['urls']['noscript_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Noscript source server'),
    '#default_value' => variable_get('doubleclick_noscript_source', DOUBLECLICK_DEFAULT_NOSCRIPT_SOURCE),
  );
  $form['urls']['ad_href'] = array(
    '#type' => 'textfield',
    '#title' => t('Advertisement redirection server'),
    '#default_value' => variable_get('doubleclick_ad_href', DOUBLECLICK_DEFAULT_AD_HREF),
  );

  return $form;
}

/**
 * Submit handler for settings form
 */
function doubleclick_admin_settings_form_submit($form, &$form_state) {
  variable_set('doubleclick_sitename', $form_state['values']['site_name_text_simple']);
  variable_set('doubleclick_ad_href', $form_state['values']['ad_href']);
  variable_set('doubleclick_noscript_source', $form_state['values']['noscript_source']);
  variable_set('doubleclick_ad_source', $form_state['values']['ad_source']);
}

/**
 * Delete slot confirmation form
 */
function build_doubleclick_admin_delete(&$form_state, $slotname) {
  $form = array(
    '#id' => 'doubleclick-admin-delete-form',
    '#submit' => array('doubleclick_admin_delete_submit'),
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete the slot from the system?'),
    'admin/content/doubleclick',
    '',
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for deleting a slot
 */
function doubleclick_admin_delete_submit($form, &$form_state) {
  $sid = arg(4);
  doubleclick_slot_delete($sid);
  $form_state['redirect'] = 'admin/content/doubleclick';
}

/**
 * Theme the slots table
 */
function doubleclick_admin_list_form() {
  $slots = doubleclick_slot_load_all();
  foreach ($slots as $slot) {
    unset($row);
    $row[] = array('data' => $slot->friendly_name);
    $row[] = array('data' => doubleclick_render_sizes($slot->sizes));
    $row[] = array('data' => $slot->position == 'default' ? 'Not specified' : $slot->position);
    $row[] = array('data' => l(t('Edit'), 'admin/content/doubleclick/edit/' . $slot->sid) . ' | ' . l(t('Delete'), 'admin/content/doubleclick/delete/' . $slot->sid));
    $rows[] = $row;
  }

  $header[] = t('Slot Name');
  $header[] = t('Size');
  $header[] = t('Position');
  $header[] = t('Operations');
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'slots')));

  return $output;
}

/**
 * Makes a slot object from submitted values
 *
 * @param array $form_state
 *  form state array from form submit handler
 */
function build_slot_object($form_state) {
  $slot = array(
    'sid' => ($form_state['values']['sid']) ? $form_state['values']['sid'] : uniqid($form_state['values']['zone_name_old']),
    'friendly_name' => trim($form_state['values']['friendly_name']),
    'zone_name' => $form_state['values']['zone_name'],
    'sizes' => $form_state['values']['size_wrapper'],
    'tile' => trim($form_state['values']['tile']),
    'position' => $form_state['values']['position'],
    'overlay' => $form_state['values']['is_overlay'],
    'kv' => $form_state['values']['kv'],
    'targeting' => array(),
    'ord' => $form_state['values']['ord'],
  );

  if (!empty($form_state['values']['kv'])) {
    $lines = preg_split("/\r\n|\n|\r/", $form_state['values']['kv']);
    foreach ($lines as $kv) {
      list($key, $val) = explode("|", $kv);
      $slot['targeting'][$key] = $val;
    }
  }

  return (object) $slot;
}

/**
 * Saves a slot to the database
 *
 * @param stdClass $slot
 *  Fully loaded slot object
 */
function doubleclick_slot_save($slot) {
  // Allow other modules to alter before saving
  // @todo: document this alter hook
  drupal_alter('doubleclick_slot_presave', $slot);

  $slots = doubleclick_slot_load_all();
  $slots[$slot->sid] = $slot;
  variable_set('doubleclick_slots', $slots);
  return $slot;
}

/**
 * Loads a slot from the database when given the sid
 *
 * @param string $sid
 *  Slot's unique ID
 * @return stdClass $slot
 *  Slot object
 */
function doubleclick_slot_load($sid) {
  $slots = doubleclick_slot_load_all();
  $slot = $slots[$sid];

  // Allow other modules to alter before loading
  // @todo: document this alter hook
  drupal_alter('doubleclick_slot_load', $slot);
  return $slot;
}

/**
 * Loads all slots from the database
 */
function doubleclick_slot_load_all() {
  return variable_get('doubleclick_slots', array());
}

/**
 * Deletes a slot
 *
 * @param string $sid
 *  Slot's unique ID
 */
function doubleclick_slot_delete($sid) {
  $slots = doubleclick_slot_load_all();
  unset($slots[$sid]);
  variable_set('doubleclick_slots', $slots);
  return TRUE;
}

/**
 * Implements hook_block_info().
 */
function doubleclick_block_info() {
  if (TRUE) {
    foreach (doubleclick_slot_load_all() as $slot) {
      $blocks[$slot->sid]['info'] = 'DoubleClick: ' . $slot->friendly_name;
      $blocks[$slot->sid]['cache'] = DRUPAL_NO_CACHE;
    }
    return $blocks;
  }
}

/**
 * Implements hook_block_view().
 */
function doubleclick_block_view($delta) {
  if (TRUE) {
    $slot = doubleclick_slot_load($delta);
    if (empty($slot)) {
      return;
    }

    $params = array(
      'source' => variable_get('doubleclick_ad_source', DOUBLECLICK_DEFAULT_AD_SOURCE),
      'noscript_source' => variable_get('doubleclick_noscript_source', DOUBLECLICK_DEFAULT_NOSCRIPT_SOURCE),
      'href' => variable_get('doubleclick_ad_href', DOUBLECLICK_DEFAULT_AD_HREF),
      'sitename' => variable_get('doubleclick_sitename', DOUBLECLICK_DEFAULT_SITENAME),
      'zone' => trim($slot->zone_name),
      'query' => array(
        'ord' => ($slot->ord) ? filter_xss($slot->ord) : rand(100, 10000),
        'sz' => doubleclick_render_sizes($slot->sizes),
      ),
    );

    foreach ($slot->targeting as $key => $value) {
      if (!empty($value)) {
        $params['query'][$key] = $value;
      }
    }
    if ($slot->position != 'default') {
      $params['query']['pos'] = $slot->position;
    }
    if (!empty($slot->tile)) {
      $params['query']['tile'] = $slot->tile;
    }
    if (!empty($slot->is_overlay)) {
      $params['query']['dcopt'] = 'ist';
    }

    // Allow other modules to alter params before we display the ad
    // @todo: document this alter hook
    drupal_alter('doubleclick_params', $params, $slot);

    $query_js = '';
    $query_nojs = '';

    foreach ($params['query'] as $key => $value) {
      $query_js .= $key . '=' . $value . ';';
      if ($key != 'dcopt' && $key != 'sz') {
        // We don't put the dcopt param in the noscript query
        $query_nojs .= $key . '=' . $value . ';';
      }
      if ($key == 'sz') {
        $query_nojs .= $key . '=' . $slot->sizes[0]['width'] . 'x' . $slot->sizes[0]['height'] . ';';
      }
    }

    $source = $params['source'] . '/' . $params['sitename'] . '/' . $params['zone'] . ';';
    $source_noscript = $params['noscript_source'] . '/' . $params['sitename'] . '/' . $params['zone'] . ';';
    $source_js = $source . $query_js;
    $source_nojs = $source_noscript . $query_nojs;
    $href = $params['href'] . '/' . $params['sitename'] . '/' . $params['zone'] . ';' . $query_nojs;
    
    $output = '<script language="JavaScript" src="' . $source_js . '" type="text/javascript"></script>';
    $output .= '<noscript>';
    $output .= '<a href="' . $href . '" target="_blank">';
    $output .= '<img src="' . $source_nojs . '" width="' . $slot->sizes[0]['width'] . '" height="' . $slot->sizes[0]['height'] . '" border="0" alt="" /></a>';
    $output .= '</noscript>';

    $block['content'] = $output;
    return $block;
  }
}

/**
 * AHAH callback for more sizes button
 */
function doubleclick_sizes_js() {
  $form_state = array(
    'storage' => NULL,
    'submitted' => FALSE,
    'current_sizes' => $_POST['size_wrapper'],
  );
  $form_build_id = $_POST['form_build_id'];
  // Get the form from the cache.
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  // We will run some of the submit handlers so we need to disable redirecting.
  $form_state['#redirect'] = FALSE;
  // We need to process the form, prepare for that by setting a few internals
  // variables.
  $form['#post'] = $_POST;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  // Build, validate and if possible, submit the form.
  drupal_process_form($form_id, $form, $form_state);
  // This call recreates the form relying solely on the form_state that the
  // drupal_process_form set up.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  // Render the new output.
  $size_form = $form['add_slot']['sizes']['size_wrapper'];
  unset($size_form['#prefix'], $size_form['#suffix']); // Prevent duplicate wrappers.
  // TODO Please change this theme call to use an associative array for the $variables parameter.
  $output = theme('status_messages') . drupal_render($size_form);

  drupal_json_output(array('status' => TRUE, 'data' => $output));
}

/**
 * Renders the sizes in format 123x456,654x321
 */
function doubleclick_render_sizes($sizes) {
  foreach ($sizes as $delta => $size) {
    if (!empty($size['width']) && !empty($size['height'])) {
      $display[] = $size['width'] . 'x' . $size['height'];
    }
  }
  return implode(',', $display);
}


/**
 * Page callback for testing
 */
function doubleclick_admin_test_page() {
  drupal_set_title('DoubleClick - Test Page');
  $output[] = t('This page displays all of the DoubleClick slots as they will appear on your site.');
  
  foreach (doubleclick_slot_load_all() as $id => $slot) {
    $block = doubleclick_block_view($slot->sid);
    $output[] = '<h3>'. $slot->friendly_name .'</h3>';
    if (module_exists('devel')) {
      $output[] = kprint_r($slot, TRUE);
    }
    $output[] = '<div class="doubleclick-preview">'. $block['content'] .'</div>';
    $output[] = '<textarea rows="8" cols="200">'. $block['content'] .'</textarea>';
  }
  
  return implode("\n", $output);
}
